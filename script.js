// Função para enviar uma mensagem
function sendMessage() {
    const messageInput = document.getElementById('message-input');
    const messageText = messageInput.value;

    // Verifica se a mensagem não está vazia (considerando apenas espaços em branco)
    if (messageText.trim() !== '') {
        // Cria um elemento de mensagem e adiciona-o ao histórico de chat
        const message = createMessageElement(messageText);
        const chatHistory = document.getElementById('chat-historyf');
        chatHistory.appendChild(message);

        // Limpa o campo de entrada após o envio da mensagem
        messageInput.value = '';
    }
}

// Função para criar um elemento de mensagem
function createMessageElement(text) {
    const message = document.createElement('div'); // Cria uma div que representa uma mensagem
    message.className = 'message'; // Define a classe da div como 'message'
    message.innerHTML = `
        <p>${text}</p>
        <div class="actions">
            <button onclick="editMessage(this)">Editar</button>
            <button onclick="deleteMessage(this)">Excluir</button>
        </div>
    `; // Insere o conteúdo da mensagem, incluindo o texto da mensagem e os botões de ação Editar e Excluir
    return message;
}

// Função para editar uma mensagem
function editMessage(editButton) {
    const message = editButton.parentNode.parentNode; // Obtém o elemento de mensagem pai do botão de edição
    const messageText = message.querySelector('p').textContent; // Obtém o texto atual da mensagem
    const newText = prompt('Editar mensagem:', messageText); // Exibe um prompt para editar o texto da mensagem

    // Verifica se o novo texto não é nulo e não consiste apenas de espaços em branco
    if (newText !== null && newText.trim() !== '') {
        message.querySelector('p').textContent = newText; // Atualiza o texto da mensagem com o novo texto inserido
    }
}

// Função para excluir uma mensagem
function deleteMessage(deleteButton) {
    const message = deleteButton.parentNode.parentNode; // Obtém o elemento de mensagem pai do botão de exclusão
    message.remove(); // Remove o elemento de mensagem do histórico de chat
}